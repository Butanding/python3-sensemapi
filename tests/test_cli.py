# system modules
import unittest

# internal modules
import sensemapi.cli.commands.main as main

# external modules
from click.testing import CliRunner


class CliTest(unittest.TestCase):
    def setUp(self):
        self.runner = CliRunner()


class CallTest(CliTest):
    def test_help(self):
        self.runner.invoke(main.cli)
