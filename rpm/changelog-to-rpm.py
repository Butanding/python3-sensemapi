#!/usr/bin/env python3
import sys
import datetime
import itertools
import re
import subprocess

logentries = re.findall(
    string=sys.stdin.read(),
    pattern=r"(v?[0-9.]+)\s*\S*(\d{4}-\d{2}-\d{2})\W+\n{2}(.*?)(?:\n{2}|\n*$)",
    flags=re.DOTALL,
)
for (versionstr, datestr, description) in logentries:
    date = datetime.datetime.strptime(datestr, "%Y-%m-%d")
    version = re.sub(string=versionstr, pattern="[^0-9.]", repl="")
    author = (
        subprocess.check_output(
            ["git", "log", "--no-walk", "--pretty=%an", versionstr]
        )
        .decode()
        .strip()
    )
    email = (
        subprocess.check_output(
            ["git", "log", "--no-walk", "--pretty=%ae", versionstr]
        )
        .decode()
        .strip()
    )
    print(
        "* {date} {author} <{email}> - {version}\n{description}".format(
            date=date.strftime("%a %b %d %Y"),
            author=author,
            email=email,
            version=version,
            description=description,
        )
    )
