# system modules
import logging
import os
import random
import textwrap
import unittest

import numpy as np
import pandas as pd

# external modules
import requests

from sensemapi import paths

# internal modules
from sensemapi.client import SenseMapClient

API_SERVER = os.environ.get("SENSEMAPI_TEST_API", paths.OPENSENSEMAP_API_TEST)


def random_id():
    return "".join(random.sample("abcdef0123456789" * 2, 25))


class SenseMapClientBaseTest(unittest.TestCase):
    def setUp(self):
        self.client = SenseMapClient(api=API_SERVER)


class SenseMapConnectionTest(SenseMapClientBaseTest):
    def test_connection_timeout(self):
        self.client.connection_timeout = 0.00001
        self.client.response_timeout = 0.00001
        with self.assertRaises(
            (requests.ReadTimeout, requests.ConnectTimeout)
        ):
            self.client.get_box(random_id())


class SenseMapClientUploadTest(SenseMapClientBaseTest):
    ids = [random_id() for i in range(5)]
    data = pd.DataFrame.from_dict(
        {
            "height": [7, 6, np.nan, 100, np.nan],
            "sensor_id": ids,
            "value": [3, 2, 1, 0, -1],
            "lat": [np.nan, 22, 33, 44, np.nan],
            "lon": [np.nan, np.nan, 33, 55, 66],
            "time": pd.date_range(
                start="2018-08-18", end="2018-08-20", periods=5
            ),
        }
    )
    data["time"].at[len(data.index) - 1] = pd.NaT
    csv = textwrap.dedent(
        """
        {3},0,2018-08-19T12:00:00Z,55.0,44.0,100.0
        {2},1,2018-08-19T00:00:00Z,33.0,33.0
        {0},3,2018-08-18T00:00:00Z
        {1},2,2018-08-18T12:00:00Z
        {4},-1
        """.format(
            *ids
        )
    ).lstrip()

    def test_dataframe_to_csv_for_upload_raises_at_duplicate_ids(self):
        data = self.data.copy()
        L = list(data["sensor_id"])
        data["sensor_id"] = L[:2] + L[:2] + L[1:2]
        with self.assertRaises(ValueError):
            self.client.dataframe_to_csv_for_upload(data)

    def test_dataframe_to_csv_for_upload(self):
        with self.assertLogs(
            logger=logging.getLogger("sensemapi.client"), level=logging.WARNING
        ) as logwarning:
            self.assertEqual(
                self.client.dataframe_to_csv_for_upload(
                    self.data, discard_incomplete=True
                ),
                self.csv,
            )
        self.assertTrue(any("incomplete" in log for log in logwarning.output))

    def test_dataframe_to_csv_for_upload_raises(self):
        with self.assertLogs(
            logger=logging.getLogger("sensemapi.client"), level=logging.WARNING
        ) as logwarning:
            with self.assertRaises(ValueError):
                self.client.dataframe_to_csv_for_upload(self.data)
        self.assertTrue(any("incomplete" in log for log in logwarning.output))
